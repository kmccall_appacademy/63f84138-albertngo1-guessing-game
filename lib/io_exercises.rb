# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game

  computer_guess = rand(1..100)

  guesses = 0

  while true
    puts "Guess a number"
    guess = gets.chomp.to_i
    puts "You guessed #{guess}"
    guesses += 1

      if guess > computer_guess
        puts "You guessed too high, try again"
      elsif guess < computer_guess
        puts "You guessed too low, try again"
      else
        puts "You guessed the correct number, #{guess} in #{guesses} guesses"
        break
      end
  end

end

def shuffled_file(filename)

  base = File.basename(filename, ".*")
  extension = File.extname(filename)
  File.open("{#{base}}-shuffled.#{extension}", "w") do |f|
    File.readlines(filename).shuffle.each do |line|
      f.puts line.chomp
    end
  end


end



if $PROGRAM_NAME == __FILE__
  if ARGV.length == 1
    shuffled_file(ARGV.first)
  else
    puts "Enter filename"
    filename = gets.chomp
    shuffled_file(filename)
  end
end
